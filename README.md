# bash_bmp_tool

A bash script that creates a heatmapped grayscale bitmap as a valid bmp file from a csv file

## Why?

A friend dared me

## What does it do?

- A bmp file header which can use any of python's matplotlib's pyplot's colormaps to map 8 bit values to RGB values is created
- Raw data is taken from a CSV file with a header (which is skipped) and with columns like "row_number,x,y,decimal_value"
- The data is put in a bash associative array, as bmp[x,y]=hex
- The data is then reordered (bmps contain data from bottom to top, left to right) and concatenated
- Finally the data is passed through xxd to be converted to binary and written to a file, after the header that got created
- The final result is a bmp file with 8 bits per pixel, but colourful

