#!/bin/bash
err_argument_parsing=100

usage() {
    echo "$0 <function> <input_name> [arguments]"
    echo "function is one of: "
    echo "- convert_to_heatmapped_grayscale <input_name> <heatmap_name>"
    echo "-- where input_name is the name of a csv file without file extension"
    echo "-- the csv file is expected to have a header (which will be ignored)"
    echo "-- and for columns - row_id (ignored), x, y and decimal value (0-255)"
    echo "- dump_bmp_header <input_name>, which is a bmp file"
}

convert_to_heatmapped_grayscale(){
image_name="$1"
colormap="$2"

echo -n>${image_name}_${colormap}.bmp
exec 3>> ${image_name}_${colormap}.bmp
width=$(( $(cat $image_name.csv | cut -d "," -f 2 | sort -u | wc -l) - 1 ))
height=$(( $(cat $image_name.csv | cut -d "," -f 3 | sort -u | wc -l) - 1 ))
width_for_size=$width
[[ "$(($width % 4))" -ne "0" ]] && width_for_size=$(( $width + 4 - ($width % 4) ))
bmp_header_creator () {
    echo -n "424d" #bmp signature
    printf %08x $((($3 * $2) + (27 * 2) + (256 * 4))) | awk '{for (i=7;i>=1;i=i-2) printf "%s%s",substr($1,i,2),(i>1?"":"")}' #bmp size, little-endian
    echo -ne "00000000" #reserved
    printf %08x $(( (256 * 4) + (27 * 2) )) | awk '{for (i=7;i>=1;i=i-2) printf "%s%s",substr($1,i,2),(i>1?"":"")}' #offset until data
    echo -ne "28000000" #header size
    #printf %08x $((27 * 2)) | awk '{for (i=7;i>=1;i=i-2) printf "%s%s",substr($1,i,2),(i>1?"":"")}' #header size
    printf %08x $1 | awk '{for (i=7;i>=1;i=i-2) printf "%s%s",substr($1,i,2),(i>1?"":"")}' #width, little-endian
    printf %08x $2 | awk '{for (i=7;i>=1;i=i-2) printf "%s%s",substr($1,i,2),(i>1?"":"")}' #height, little-endian
    echo -ne "01000800" #planes (should be 1), bitcount
    echo -ne "00000000" #compression
    echo -ne "00000000" #size image
    echo -ne "00000000" #XPerlPerMeter
    echo -ne "00000000" #YPerlPerMeter
    echo -ne "00000000" #colours used
    echo -ne "00000000" #colour important
    python3 <(cat << EOF
import matplotlib.pyplot as plt
for i in range (256):
  for j in range(3):
    print("{:02x}".format(int(plt.get_cmap('$colormap')(i)[j]*255)), end = "")
  print("00", end = "")
exit(0)
EOF
    ) #used to map 8bpp grayscale to colourful heatmap
#    for i in $(seq 0 255);do
#        [[ "$i" -eq "0" ]] && echo -n "ff000000" && continue
#        [[ "$i" -eq "255" ]] && echo -n "0000ff00" && continue
#        for j in $(seq 0 2); do printf %02x $i; done; echo -n 00
#    done
}
bmp_header_creator $width $height $width_for_size | xxd -r -p >&3

echo -n doing csv parsing
declare -A bmp
first=0
time while IFS=$',\n\r' read -r row_id x y value;do
[[ "$first" -eq "0" ]] && first=1 && continue
bmp["${x},${y}"]=$(printf "%02x" ${value%\.*})
#echo value $value $(printf "%02x" ${value%\.*})
#echo bmp["${x},${y}"] $(printf "%02x" ${value%\.*})
done < <(cat $image_name.csv ; echo )

echo
echo -n dumping bitmap data
bmp_raw=""
time for y in $(seq $(($height-1)) -1 0) ; do
for x in $(seq 0 $(($width-1))) ; do
bmp_raw="${bmp_raw}${bmp[$x,$y]}"
done
if [[ "$(($width_for_size - $width))" -ne "0" ]]
then
    for i in $(seq 1 $(($width_for_size - $width)))
    do
        echo adding zeroes at y $y and x is $x
        bmp_raw="${bmp_raw}00"
    done
fi
done
echo "${bmp_raw}" | xxd -r -p >&3
exec 3>&-
convert ${image_name}_${colormap}.bmp ${image_name}_${colormap}.png
echo
echo -n overall time
}

dump_bmp_header() {
    signature=$(head -c 2)
    echo signature is $signature
    bmp_size=$(head -c 4 | xxd -p)
    echo -n "bmp_size hex $bmp_size and dec "
    echo $((16#$(echo "$bmp_size" | awk '{for (i=7;i>=1;i=i-2) printf "%s%s",substr($1,i,2),(i>1?"":"")}')))
    head -c 4 > /dev/null #"reserved" and ignored, probably 00000000
    offset=$(head -c 4 | xxd -p)
    echo -n "offset hex $offset and dec "
    echo $((16#$(echo "$offset" | awk '{for (i=7;i>=1;i=i-2) printf "%s%s",substr($1,i,2),(i>1?"":"")}')))
    header_size=$(head -c 4 | xxd -p)
    echo -n "header_size hex $header_size and dec "
    echo $((16#$(echo "$header_size" | awk '{for (i=7;i>=1;i=i-2) printf "%s%s",substr($1,i,2),(i>1?"":"")}')))
    width=$(head -c 4 | xxd -p)
    echo -n "width hex $width and dec "
    echo $((16#$(echo "$width" | awk '{for (i=7;i>=1;i=i-2) printf "%s%s",substr($1,i,2),(i>1?"":"")}')))
    height=$(head -c 4 | xxd -p)
    echo -n "height hex $height and dec "
    echo $((16#$(echo "$height" | awk '{for (i=7;i>=1;i=i-2) printf "%s%s",substr($1,i,2),(i>1?"":"")}')))
    planes=$(head -c 2 | xxd -p)
    echo -n "planes (should be 1) hex $planes and dec "
    echo $((16#$(echo "$planes" | awk '{for (i=7;i>=1;i=i-2) printf "%s%s",substr($1,i,2),(i>1?"":"")}')))
    bitcount=$(head -c 2 | xxd -p)
    echo -n "bitcount hex $bitcount and dec "
    echo $((16#$(echo "$bitcount" | awk '{for (i=7;i>=1;i=i-2) printf "%s%s",substr($1,i,2),(i>1?"":"")}')))
    compression=$(head -c 4 | xxd -p)
    echo -n "compression (0 meeans no compression) hex $compression and dec "
    echo $((16#$(echo "$compression" | awk '{for (i=7;i>=1;i=i-2) printf "%s%s",substr($1,i,2),(i>1?"":"")}')))
    size_image=$(head -c 4 | xxd -p)
    echo -n "size_image hex $size_image and dec "
    echo $((16#$(echo "$size_image" | awk '{for (i=7;i>=1;i=i-2) printf "%s%s",substr($1,i,2),(i>1?"":"")}')))
    x_per_meter=$(head -c 4 | xxd -p)
    echo -n "x_per_meter hex $x_per_meter and dec "
    echo $((16#$(echo "$x_per_meter" | awk '{for (i=7;i>=1;i=i-2) printf "%s%s",substr($1,i,2),(i>1?"":"")}')))
    y_per_meter=$(head -c 4 | xxd -p)
    echo -n "y_per_meter hex $y_per_meter and dec "
    echo $((16#$(echo "$y_per_meter" | awk '{for (i=7;i>=1;i=i-2) printf "%s%s",substr($1,i,2),(i>1?"":"")}')))
    colours_used=$(head -c 4 | xxd -p)
    echo -n "colours_used hex $colours_used and dec "
    echo $((16#$(echo "$colours_used" | awk '{for (i=7;i>=1;i=i-2) printf "%s%s",substr($1,i,2),(i>1?"":"")}')))
    colours_important=$(head -c 4 | xxd -p)
    echo -n "colours_important hex $colours_important and dec "
    echo $((16#$(echo "$colours_important" | awk '{for (i=7;i>=1;i=i-2) printf "%s%s",substr($1,i,2),(i>1?"":"")}')))
}

dump_bmp_colortable() {
    header="$(dump_bmp_header < $1)"
    offset=$(echo "$header" | grep offset | cut -d " " -f 6)
    header_size=$(echo "$header" | grep header_size | cut -d " " -f 6)
    echo offset is $offset and header_size + 14 is $(($header_size + 14))
    head -c "$(($offset))" < $1 | tail -c "$((256 * 4))" | xxd -p -c 4
}

dump_bmp_to_csv() {
	echo -n > $1.csv
	exec 3>> $1.csv
    header="$(dump_bmp_header < $1)"
    offset=$(echo "$header" | grep offset | cut -d " " -f 6)
    bmp_size=$(echo "$header" | grep bmp_size | cut -d " " -f 6)
    width=$(echo "$header" | grep width | cut -d " " -f 6)
    height=$(echo "$header" | grep height | cut -d " " -f 6)
	echo doing bmp parsing, offset $offset, bmp_size $bmp_size, width $width, height $height
	echo " ,X,Y,Value" >&3
	declare -A bmp
	for y1 in $(seq $((${height} - 1)) -1 0); do
		for x1 in $(seq 0 $((${width} - 1))); do
			#echo do stuff with x $x and y $y
			curr_value=$(head -c 1 | xxd -p)
			#echo curr_value is $curr_value and dec $((16#${curr_value}))
			bmp["${x1},${y1}"]=${curr_value}
			echo bmp["${x1},${y1}"] is ${bmp["${x1},${y1}"]}
			#echo "${iterator},${x1},${y1},$((16#${curr_value}))" >&3
		done
	done < <(cat $1 | tail -c $((${bmp_size} - ${offset})) )
	
	iterator=0
	for y2 in $(seq 0 $((${height} - 1))); do
		for x2 in $(seq 0 $((${width} - 1))); do
			iterator=$((${iterator} + 1))
			#echo doing ${iterator},${x2},${y2},$((16#${bmp[${x2},${y2}]}))
			echo ${iterator},${x2},${y2},$((16#${bmp[${x2},${y2}]})) >&3
		done
	done
	exec 3>&-
# echo -n>${image_name}_${colormap}.bmp
# exec 3>> ${image_name}_${colormap}.bmp
# exec 3>&-

	
	# first=0
	# time while IFS=$',\n\r' read -r row_id x y value;do
	# [[ "$first" -eq "0" ]] && first=1 && continue
	# bmp["${x},${y}"]=$(printf "%02x" ${value%\.*})
	# #echo value $value $(printf "%02x" ${value%\.*})
	# echo bmp["${x},${y}"] $(printf "%02x" ${value%\.*})
	# done < <(cat $image_name.bmp | tail -c $((${size} - ${offset})) )
}

while : ; do
case "$1" in
    convert_to_heatmapped_grayscale )
        time convert_to_heatmapped_grayscale $2 $3
        shift 3
        break
        ;;
    dump_bmp_header )
        time dump_bmp_header < $2.bmp
        shift 2
        break
        ;;
    dump_bmp_colortable )
        time dump_bmp_colortable $2.bmp
        shift 2
        break
        ;;
    dump_bmp_to_csv )
        time dump_bmp_to_csv $2.bmp
        shift 2
        break
        ;;
    -h | --help | help )
        usage
        exit 0
        ;;
    --)
        shift;
        break
        ;;
    * )
        echo "argument parsing failed" 2>&1
        usage 2>&1
        exit $err_argument_parsing
        ;;
esac; done
